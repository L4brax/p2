angular.module('starter.controllers').controller('InfoCtrl', function ($scope, $stateParams, $rootScope, $state) {
	/*$scope.setUser = user => {
		if (user.id !== undefined && user.id > 0) {
			console.log("AA");
			$rootScope.user = user;
		} else {
			console.log(user.prenom);
			$rootScope.data.maxId++;
			if (user.prenom === "" || user.prenom === undefined || user.prenom === null) {
				console.log("prenom x");
				$rootScope.user.prenom = "Nouvel utilisateur";
				$rootScope.user.persoId = 1;
				$rootScope.user.id = $rootScope.data.maxId;
			}
			$rootScope.data.users.push($rootScope.user);
		}
		$state.go('app.users');
	}*/
	$scope.$on('$ionicView.enter', function (e) {
		if ($stateParams.user == 'new') {
			$scope.user = {
				id: -1,
				prenom: null,
				protocole: {
					petitDejeuner: {
						glucideLent: null,
						glucideRapide: null
					},
					dejeuner: {
						glucideLent: null,
						glucideRapide: null
					},
					gouter: {
						glucideLent: null,
						glucideRapide: null
					},
					diner: {
						glucideLent: null,
						glucideRapide: null
					}
				},
				avatarId: 1,
				persoId: 1
			};
		} else {
			$scope.user = $rootScope.data.users.find(user => user.id == $stateParams.user);
		}
		console.log($rootScope.user)
	});

	$scope.setUser = function () {
		console.log($stateParams);
		if ($scope.user.id !== undefined && $scope.user.id > 0) {
			console.log("user modified");
			$scope.saveExistingUser($scope.user);
		} else {
			console.log($scope.user.prenom + ' is new');
			$scope.saveNewUser($scope.user);
			// $rootScope.data.userMaxId++;
			// if (user.prenom === "" || user.prenom === undefined || user.prenom === null) {
			// 	console.log("prenom x");
			// 	$rootScope.user.prenom = "Nouvel utilisateur";
			// 	$rootScope.user.persoId = 1;
			// 	$rootScope.user.id = $rootScope.data.maxId;
			// }
			// $rootScope.data.users.push($rootScope.user);
		}
		$state.go('app.users');
	};

	$scope.saveExistingUser = function (user) {
		let index = -1;
		angular.forEach($rootScope.data.users, function (userData, key) {
			if (userData.id === user.id) {
				index = key;
			}
		});
		if (index >= 0) {
			$rootScope.data.users[index] = user;
		} else {
			$scope.saveNewUser(user);
		}
	};

	$scope.saveNewUser = function (user) {
		$rootScope.data.usersIdMax++;
		user.id = $rootScope.data.usersIdMax;
		$rootScope.data.users.push($scope.user);
	};

});
