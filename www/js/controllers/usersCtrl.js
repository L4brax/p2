angular.module('starter.controllers')

	.controller('UsersCtrl', function ($scope, $stateParams, $rootScope, $state) {
		$scope.users = $rootScope.data.users;
		if ($scope.users.length < 1) {
			$state.go("app.register");
		}

		$scope.setUser = user => {
			$rootScope.user = user;
			$state.go('app.repas');
			//console.log($rootScope.user);
		};

		$scope.$on('$ionicView.enter', function (e) {
			console.log($rootScope.data.users)
		});
		$scope.addUser = () => {
			$rootScope.user = {};
			// 	id: 0,
			// 	prenom: "",
			// 	protocole: {
			// 		petitDejeuner: {
			// 			glucideLent: undefined,
			// 			glucideRapide: undefined
			// 		},
			// 		dejeuner: {
			// 			glucideLent: undefined,
			// 			glucideRapide: undefined
			// 		},
			// 		gouter: {
			// 			glucideLent: undefined,
			// 			glucideRapide: undefined
			// 		},
			// 		diner: {
			// 			glucideLent: undefined,
			// 			glucideRapide: undefined
			// 		}
			// 	}
			// }
			$state.go('app.infos');
		};

		$scope.editUser = (userp) => {
			$rootScope.user = userp;
			$state.go('app.infos');
		}

		$scope.showHistory = user => {
			$rootScope.user = user;
			$state.go('app.historique');
		};
	});
