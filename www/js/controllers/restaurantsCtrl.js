angular.module('starter.controllers')

    .controller('RestaurantsCtrl', function ($scope, $rootScope, $stateParams) {
        $scope.restos = $rootScope.data.restos;
        $scope.repasId = $stateParams.repasId;
    });