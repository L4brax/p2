angular.module('starter.controllers').controller('ItemsCtrl', function ($scope, $state, $rootScope, $stateParams, $ionicScrollDelegate) {
  var dataAllItems = $rootScope.data.items;
  //var currentRestoId = 1;//to delete for prod (uncomment under line)
  $scope.currentRestoId = parseInt($stateParams.restoId);
  $scope.repasId = $stateParams.repasId;
  // $scope.currentRestoId = $stateParams.restoId;
  //item example
  // {
  //     "id": "1",
  //     "nom": "BigMac",
  //     "type": "1",
  //     "glucideLent": "",
  //     "glucideRapide": "",
  //     "restoId": "1",
  //     "photoLink": "./img/items/bigmac.png"
  // }


  $scope.data = {};
  $scope.data.plate = [];
  $scope.myType = 1;
  $scope.items = [];
  $scope.nbProtocoles = 0;

  $scope.allItems = dataAllItems.filter(function (item) { return item.restoId === $scope.currentRestoId });

  $scope.width = { P1: { width: "0%" }, P2: { width: "0%" }, P3: { width: "0%" } };


  $scope.itemFilter = function (myType) {
    $scope.myType = myType;
    $scope.items = $scope.allItems.filter(function (item) { return item.type === $scope.myType });
  };

  $scope.itemFilter(1);

  $scope.addInPlate = function (itemId) {
    item = $scope.allItems.find(item => item.id === itemId);
    item.number = $scope.data.plate.length;
    $scope.data.plate.push(item);
    $scope.setWidth();
  };

  $scope.scrollTop = function () {
    $ionicScrollDelegate.scrollTop();
  };

  //<!--ui-sref="app.bonAppetit({restoId:currentRestoId, repasId:repasId, itemList:JSON.stringify(data)})"-->
  $scope.nextState = function () {
    $rootScope.plate = $scope.data.plate;
    $state.go("app.bonAppetit", {
      restoId: $scope.currentRestoId,
      repasId: $scope.repasId
    });
  };

  let getRapide = function () {
    return $scope.data.plate.map((item) => item.glucideRapide > 0 ? item.glucideRapide : 0).reduce((a, b) => a + b, 0);
  };

  let getLent = function () {
    return $scope.data.plate.map((item) => item.glucideLent > 0 ? item.glucideLent : 0).reduce((a, b) => a + b, 0);
  };

  let getPercentToProtocole = function () {
    let cible = {};
    cible.rapide = $rootScope.user.protocole[$scope.repasId].glucideRapide;
    cible.lent = $rootScope.user.protocole[$scope.repasId].glucideLent;
    let actual = {};
    actual.rapide = getRapide();
    actual.lent = getLent();
    let percent = {};
    percent.rapide = (actual.rapide * 100) / cible.rapide;
    percent.lent = (actual.lent * 100) / cible.lent;
    return (percent.rapide + percent.lent) / 2;
  };

  $scope.setWidth = function () {
    let prct = getPercentToProtocole();
    if (prct <= 0) {
      $scope.width.P1.width = '0%';
      $scope.width.P2.width = '0%';
      $scope.width.P3.width = '0%';
      $scope.nbProtocoles = 0;
    } else if (prct <= 100) {
      $scope.width.P1.width = prct + '%';
      $scope.width.P2.width = '0%';
      $scope.width.P3.width = '0%';
      $scope.nbProtocoles = 1;
    } else if (prct <= 200) {
      $scope.width.P1.width = (200 - prct) + '%';
      $scope.width.P2.width = (prct - 100) + '%';
      $scope.width.P3.width = '0%';
      $scope.nbProtocoles = 2;
    } else if (prct <= 300) {
      $scope.width.P1.width = '0%';
      $scope.width.P2.width = (300 - prct) + '%';
      $scope.width.P3.width = (prct - 200) + '%';
      $scope.nbProtocoles = 3;
    } else {
      $scope.width.P1.width = '0%';
      $scope.width.P2.width = '0%';
      $scope.width.P3.width = '100%';
      $scope.nbProtocoles = 4;
    }
  };

});
