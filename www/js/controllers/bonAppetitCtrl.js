angular.module('starter.controllers')

    .controller('BonAppetitCtrl', function ($scope, $stateParams, $rootScope, $state) {
        $scope.restoId = $stateParams.restoId;
        $scope.repasType = $stateParams.repasId;
        $scope.totGlucides = {
            lents: 0,
            rapides: 0
        };

        angular.forEach($scope.plate, function (item) {
            $scope.totGlucides.lents += item.glucideLent;
            $scope.totGlucides.rapides += item.glucideRapide;
        });

        $scope.nextState = function () {
            $rootScope.data.plateauxIdMax += 1;
            let plateaux = {
                restoId: $scope.restoId,
                id: $rootScope.data.plateauxIdMax,
                userId: $rootScope.user.id,
                itemId: $rootScope.plate.map(function (item) { return item.id }),
                date: Date.now(),
                glucideRapide: $scope.totGlucides.rapides,
                glucideLent: $scope.totGlucides.lents
            };
            $rootScope.data.plateaux.push(plateaux);
            delete $rootScope.plate;
            $state.go("app.users");
        };
	$scope.perso = angular.copy($rootScope.data.persos.find(function(perso){return perso.id === $rootScope.user.persoId}));

    });