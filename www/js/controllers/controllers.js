angular.module('starter.controllers', [])
.run(function ($rootScope, $http) {
  //$rootScope.test = require('./data.json');

  $http.get('./data.json')
    .success(function (data) {
      // The json data will now be in rootscope.
      $rootScope.data = data;
    });
  $rootScope.user = {};
})

.controller('AppCtrl', function($scope, $ionicModal, $timeout, $state) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  $scope.userState = 'app.users';
  $scope.$on('$ionicView.enter', () => {
    $scope.state = $state.current.name;
    if ($scope.state === 'app.users') {
      $scope.homeButton = "homeButtonHidden";
    } else {
      $scope.homeButton = "homeButtonDisplay";
    }


    console.log($scope.state);
  });


});
